import React from 'react';
import Modal from './Modal';
import ModalFooter from './ModalFooter';

const ModalImage = ({ isOpen, onClose, title, content, imageUrl, confirmText }) => {
    return (
        <Modal isOpen={isOpen} onClose={onClose}>
            <div>
                <img src={imageUrl} />
                <h2>{title}</h2>
                <p>{content}</p>
                <ModalFooter
                    firstText="Cancel"
                    firstClick={onClose}
                    secondaryText={confirmText}
                />
            </div>
        </Modal>
    );
};

export default ModalImage;
